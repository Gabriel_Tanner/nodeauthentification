var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
const axios = require('axios')

var User = require('../models/user')


router.get('/test', (req, res) => {
	axios.get('http://10.10.8.90:9000/api/', {
		headers: {
			'Authorization': `Basic cfed362ff6154b260dde34ea090c90624aca6c49:`,
		},
	}).then((response) => {
		res.status(201).json(response)
	})
	
})

// Register
router.get('/register', function(req, res){
	res.render('register');
});

router.post('/register', function(req, res){
	var name = req.body.name;
	var email = req.body.email;
	var username = req.body.username;
	var password = req.body.password;
	var password2 = req.body.password2;

	// Validation
	req.checkBody('name', 'Name is required').notEmpty();
	req.checkBody('email', 'Email is required').notEmpty();
	req.checkBody('email', 'Email is not valid').isEmail();
	req.checkBody('username', 'Username is required').notEmpty();
	req.checkBody('password', 'Password is required').notEmpty();
	req.checkBody('password', 'Password is not valid').matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,}$/, "i")
	req.checkBody('password2', 'Passwords do not match').equals(req.body.password);


	var errors = req.validationErrors()
	if(errors){
		res.render('register',{errors:errors})
	}else{
		var newUser = new User({
			name: name,
			email: email,
			username: username,
			password: password
		})

		User.createUser(newUser, function(err, user){
			if(err) throw err
			console.log(user)
		})

		req.flash('success_msg', 'You are registeredd and can now login')

		res.redirect('/users/login')
	}
});

// Login
router.get('/login', function(req, res){
	res.render('login');
});

passport.serializeUser(function (user, done) {
	done(null, user.id);
});

passport.deserializeUser(function (id, done) {
	User.getUserById(id, function (err, user) {
		done(err, user);
	});
});

passport.use(new LocalStrategy(
	function (username, password, done) {
		User.getUserByUsername(username, function (err, user) {
			if (err) throw err;
			if (!user) {
				return done(null, false, { message: 'Unknown User' });
			}

			User.comparePassword(password, user.password, function (err, isMatch) {
				if (err) throw err;
				if (isMatch) {
					return done(null, user);
				} else {
					return done(null, false, { message: 'Invalid password' });
				}
			});
		});
}));

passport.serializeUser(function (user, done) {
	done(null, user.id);
});

passport.deserializeUser(function (id, done) {
	User.getUserById(id, function (err, user) {
		done(err, user);
	});
});

router.post('/login',
	passport.authenticate('local', { successRedirect: '/', failureRedirect: '/users/login', failureFlash: true }),
	function (req, res) {
		res.redirect('/');
});

router.get('/logout', function (req, res) {
	req.logout();
	req.flash('success_msg', 'You are logged out');

	res.redirect('/users/login');
});

module.exports = router;